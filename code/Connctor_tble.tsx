import * as React from "react"
import { Frame, useCycle } from "framer"
import clsx from "clsx"
import PropTypes from "prop-types"
import { loadCSS } from "fg-loadcss"
import { makeStyles } from "@material-ui/core/styles"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TablePagination from "@material-ui/core/TablePagination"
import TableRow from "@material-ui/core/TableRow"
import TableSortLabel from "@material-ui/core/TableSortLabel"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"
import Paper from "@material-ui/core/Paper"
import Checkbox from "@material-ui/core/Checkbox"
import SearchIcon from "@material-ui/icons/Search"
import InputBase from "@material-ui/core/InputBase"
import Delete from "@material-ui/icons/Delete"
import Add from "@material-ui/icons/Add"
import Button from "@material-ui/core/Button"
import Icon from "@material-ui/core/Icon"
import Divider from "@material-ui/core/Divider"

// Open Preview (CMD + P)
// API Reference: https://www.framer.com/api

export function Connctor_tble() {
    const [twist, cycle] = useCycle(
        { scale: 0.5, rotate: 0 },
        { scale: 1, rotate: 90 }
    )

    return <Frame animate={twist} onTap={() => cycle()} size={"100%"} />
}
import React from "react"

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1
    }
    if (b[orderBy] > a[orderBy]) {
        return 1
    }
    return 0
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index])
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0])
        if (order !== 0) return order
        return a[1] - b[1]
    })
    return stabilizedThis.map(el => el[0])
}

function getSorting(order, orderBy) {
    return order === "desc"
        ? (a, b) => desc(a, b, orderBy)
        : (a, b) => -desc(a, b, orderBy)
}

const TableHead = props => {
    const {
        onSelectAllClick,
        order,
        orderBy,
        numSelected,
        rowCount,
        onRequestSort,
    } = props
    const createSortHandler = property => event => {
        onRequestSort(event, property)
    }

    return (
        <TableRow>
            <TableCell padding="checkbox">
                <Checkbox
                    indeterminate={numSelected > 0 && numSelected < rowCount}
                    checked={numSelected === rowCount}
                    onChange={onSelectAllClick}
                    inputProps={{ "aria-label": "Select all" }}
                />
            </TableCell>
            {props.headRows.map(row => (
                <TableCell
                    key={row.id}
                    align={row.numeric ? "right" : "left"}
                    padding={row.disablePadding ? "none" : "default"}
                    sortDirection={orderBy === row.id ? order : false}
                >
                    <TableSortLabel
                        active={orderBy === row.id}
                        direction={order}
                        onClick={createSortHandler(row.id)}
                    >
                        {row.label}
                    </TableSortLabel>
                </TableCell>
            ))}
        </TableRow>
    )
}

TableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
    headRows: PropTypes.arrayOf(PropTypes.object).isRequired,
}

const useConnectorsOverviewTableStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        backgroundColor: theme.palette.secondary.light,
    },
    buttonToolBar: {
        backgroundColor: "#fff",
    },
    toolBarButton: {
        marginRight: "10px",
    },
    paper: {
        width: "100%",
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    tableWrapper: {
        overflowX: "auto",
    },
    leftIcon: {
        marginRight: theme.spacing(1),
    },
}))

export const ConnectorsOverviewTable = props => {
    const classes = useConnectorsOverviewTableStyles()

    // initialize state
    const [order, setOrder] = React.useState("asc")
    const [orderBy, setOrderBy] = React.useState("calories")
    const [selected, setSelected] = React.useState([])
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(5)
    const [query, setQuery] = React.useState("")

    // FontAwesome imports
    React.useEffect(() => {
        loadCSS(
            "https://use.fontawesome.com/releases/v5.1.0/css/all.css",
            document.querySelector("#font-awesome-css")
        )
    }, [])

    const headRows = [
        { id: "name", numeric: false, disablePadding: true, label: "name" },
        { id: "ip", numeric: false, disablePadding: true, label: "ip" },
        { id: "url", numeric: false, disablePadding: true, label: "url" },
        { id: "active", numeric: false, disablePadding: true, label: "status" },
    ]
    const createRow = connector => {
        const name = connector.name
        const ip = connector.ip
        const url = connector.url
        const active = connector.active
        return { name, ip, url, active }
    }

    const rows = props.connectors.map(connector => {
        return createRow(connector)
    })

    const handleRequestSort = (event, property) => {
        const isDesc = orderBy === property && order === "desc"
        setOrder(isDesc ? "asc" : "desc")
        setOrderBy(property)
    }

    const handleSelectAllClick = event => {
        if (event.target.checked) {
            const newSelecteds = rows.map(n => n.name)
            setSelected(newSelecteds)
            return
        }
        setSelected([])
    }

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name)
        let newSelected = []

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name)
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1))
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1))
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1)
            )
        }
        setSelected(newSelected)
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage)
    }

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(event.target.value)
        setPage(0)
    }

    const isSelected = name => selected.indexOf(name) !== -1

    const handleQueryChange = event => {
        setQuery(event.target.value)
        props.handleQueryChange(event.target.value)
    }

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Toolbar className={classes.root}>
                    <Typography className={classes.title} variant="h6" noWrap>
                        Connectors
                    </Typography>
                </Toolbar>
                <Toolbar className={classes.buttonToolBar}>
                    <Button
                        variant="contained"
                        onClick={props.handleDeleteClick}
                        className={classes.toolBarButton}
                    >
                        <Delete className={classes.leftIcon} />
                        Remove
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={props.handleAddClick}
                        className={classes.toolBarButton}
                    >
                        <Add className={classes.leftIcon} />
                        Add connector
                    </Button>
                </Toolbar>
                <Divider />
                <div className={classes.tableWrapper}>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={"medium"}
                    >
                        <TableBody>
                            <TableRow rowSpan={4}>
                                <TableCell colSpan={1} size={"small"}>
                                    <div className={classes.search}>
                                        <SearchIcon />
                                    </div>
                                </TableCell>
                                <TableCell
                                    colSpan={4}
                                    padding="none"
                                    size={"small"}
                                >
                                    <InputBase
                                        placeholder="Search…"
                                        inputProps={{ "aria-label": "Search" }}
                                        fullWidth
                                        value={query}
                                        onChange={handleQueryChange}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={handleSelectAllClick}
                                onRequestSort={handleRequestSort}
                                rowCount={rows.length}
                                headRows={headRows}
                            />
                            {stableSort(rows, getSorting(order, orderBy))
                                .slice(
                                    page * rowsPerPage,
                                    page * rowsPerPage + rowsPerPage
                                )
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.name)
                                    const labelId = `enhanced-table-checkbox-${index}`
                                    return (
                                        <TableRow
                                            hover
                                            onClick={event =>
                                                handleClick(event, row.name)
                                            }
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.name}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    checked={isItemSelected}
                                                    inputProps={{
                                                        "aria-labelledby": labelId,
                                                    }}
                                                />
                                            </TableCell>
                                            <TableCell
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                padding="none"
                                            >
                                                {row.name}
                                            </TableCell>
                                            <TableCell padding="none">
                                                {row.ip}
                                            </TableCell>
                                            <TableCell padding="none">
                                                {row.url}
                                            </TableCell>
                                            <TableCell>
                                                {row.active === "true" ? (
                                                    <Icon
                                                        className={clsx(
                                                            classes.icon,
                                                            "fa fa-check-circle"
                                                        )}
                                                        color="primary"
                                                    />
                                                ) : (
                                                    <Icon
                                                        className={clsx(
                                                            classes.icon,
                                                            "fas fa-plus-circle"
                                                        )}
                                                        color="secondary"
                                                    />
                                                )}
                                            </TableCell>
                                        </TableRow>
                                    )
                                })}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        "aria-label": "Previous Page",
                    }}
                    nextIconButtonProps={{
                        "aria-label": "Next Page",
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    )
}

ConnectorsOverviewTable.propTypes = {
    connectors: PropTypes.arrayOf(PropTypes.object).isRequired,
    handleAddClick: PropTypes.func.isRequired,
    handleDeleteClick: PropTypes.func.isRequired,
    handleQueryChange: PropTypes.func.isRequired,
}
